package ru.t1.strelcov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.model.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    void add(@NotNull final E user);

    void clear();

    void remove(@NotNull final E user);

    @Nullable
    E findById(@NotNull final String id);

    @Nullable
    E removeById(@NotNull final String id);

}
