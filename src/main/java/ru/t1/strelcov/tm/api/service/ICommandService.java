package ru.t1.strelcov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    AbstractCommand getCommandByName(@NotNull String name);

    @NotNull
    AbstractCommand getCommandByArg(@NotNull String arg);

    void add(@Nullable AbstractCommand command);

}
