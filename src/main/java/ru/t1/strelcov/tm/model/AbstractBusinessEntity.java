package ru.t1.strelcov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.entity.IWBS;
import ru.t1.strelcov.tm.enumerated.Status;

import java.util.Date;

import static ru.t1.strelcov.tm.enumerated.Status.NOT_STARTED;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractBusinessEntity extends AbstractEntity implements IWBS {

    @Nullable
    private String name = "";

    @Nullable
    private String description = "";

    @NotNull
    private Status status = NOT_STARTED;

    @NotNull
    private Date created = new Date();

    @Nullable
    private String userId;

    @Nullable
    private Date dateStart;

    public AbstractBusinessEntity(@Nullable String userId, @Nullable String name) {
        this.userId = userId;
        this.name = name;
    }

    public AbstractBusinessEntity(@Nullable String userId, @Nullable String name, @Nullable String description) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return id + " : " + name + " : " + created + " : " + userId;
    }

}
