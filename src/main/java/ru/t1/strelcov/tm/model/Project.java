package ru.t1.strelcov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractBusinessEntity {

    public Project(@Nullable String userId, @Nullable String name) {
        super(userId, name);
    }

    public Project(@Nullable String userId, @Nullable String name, @Nullable String description) {
        super(userId, name, description);
    }

}
