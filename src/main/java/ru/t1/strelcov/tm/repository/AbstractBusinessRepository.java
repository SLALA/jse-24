package ru.t1.strelcov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IBusinessRepository;
import ru.t1.strelcov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> implements IBusinessRepository<E> {

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return list.stream()
                .filter((entity) -> userId.equals(entity.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return list.stream()
                .filter((entity) -> userId.equals(entity.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<E> entities = findAll(userId);
        list.removeAll(entities);
    }

    @Nullable
    @Override
    public E findByName(@NotNull final String userId, @NotNull final String name) {
        return list.stream()
                .filter((entity) -> userId.equals(entity.getUserId()) && name.equals(entity.getName()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<E> entities = findAll(userId);
        return entities.stream().skip(index).findFirst().orElse(null);
    }

    @Nullable
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) {
        return list.stream()
                .filter((entity) -> userId.equals(entity.getUserId()) && id.equals(entity.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findById(userId, id));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Nullable
    @Override
    public E removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findByName(userId, name));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Optional<E> entity = Optional.ofNullable(findByIndex(userId, index));
        entity.ifPresent(this::remove);
        return entity.orElse(null);
    }

}
