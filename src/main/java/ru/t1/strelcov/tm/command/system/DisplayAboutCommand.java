package ru.t1.strelcov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.command.AbstractCommand;

public final class DisplayAboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Sla La");
        System.out.println("slala@slala.ru");
    }

}
