package ru.t1.strelcov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.service.IProjectService;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.util.TerminalUtil;

import static ru.t1.strelcov.tm.enumerated.Status.COMPLETED;

public final class ProjectCompleteByNameCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-complete-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Complete project by name.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        final IProjectService projectService = serviceLocator.getProjectService();
        System.out.println("[COMPLETE TASK BY NAME]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.changeStatusByName(userId, name, COMPLETED);
        showProject(project);
    }

}
